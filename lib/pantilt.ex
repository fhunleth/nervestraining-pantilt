defmodule Pantilt do
  @moduledoc """
  Documentation for Pantilt.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Pantilt.hello
      :world

  """
  def hello do
    :world
  end
end
